class Plant:
    # Método construtor da classe Plant
    def __init__(self, input_up_speed: int, input_down_speed: int, input_height: int):
        self.input_up_speed = input_up_speed
        self.input_down_speed = input_down_speed
        self.input_height = input_height
        self.total_days = 1
        self.final_growth = 0

    # Método de crescimento da planta
    def growing_plant(self) -> int:
        # Laço que repetirá a altura setada não for alcançada
        while self._not_grow():
            # Crescimento final será incrementado pela velocidade de crescimento diário
            self.final_growth += self.input_up_speed
            # Se atingir a altura setada, o laço de repetição termina
            if not self._not_grow():
                break
            # Crescimento final será decrementado pelo declínio noturno
            self.final_growth -= self.input_down_speed
            # Acrescenta um dia ao total de dias para atingir a altura setada e a retorna
            self.total_days += 1
        return self.total_days

    # Método que verifica se a altura setada foi alcançada
    def _not_grow(self) -> bool:
        if self.final_growth >= self.input_height:
            return False
        return True


# Manual test
# plant = Plant(100, 10, 910)
# print(plant.growing_plant())
