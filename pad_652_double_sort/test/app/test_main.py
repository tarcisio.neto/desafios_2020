import unittest
from pad_652_double_sort.app.main import double_sort


class TestMain(unittest.TestCase):
    def test_double_sort(self):
        self.assertEqual(double_sort(
            ["Banana", "Orange", '.', "Apple", 3.5, "Mango", 0, 2, 2]),
            [0, 2, 2, 3.5, '.', "Apple", "Banana", "Mango", "Orange"])

    def test_function_double_sort_return_integer(self):
        self.assertIsInstance(double_sort([12, "Mountain Bike", 3, "Speed", 2.5, "Fixed Gear"]), list)
