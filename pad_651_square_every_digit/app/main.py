def square_every_digit(number: int) -> int:
    num_list = list(str(number))
    square_list = []
    final_number = 0
    # Percorrerá a lista de números
    for n in num_list:
        # Realizará a potencialização de cada número
        square = int(n)**2
        # Adicionará a lista estes números
        square_list.append(str(square))
        # Variável "número final" receberá estes números da lista, agrupando-os
        final_number = int(''.join(square_list))
    return final_number


print(square_every_digit(2541))
