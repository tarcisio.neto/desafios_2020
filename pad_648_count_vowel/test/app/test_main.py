import unittest
from pad_648_count_vowel.app.main import vowel_count


class TestMain(unittest.TestCase):
    def test_vowel_count(self):
        self.assertEqual(vowel_count("As melhores empresas para trabalhar tem as melhores pessoas."), 19)
        self.assertEqual(vowel_count("Tamo junto é mergulhar de corpo e alma em cada projeto, apaixonados pelo o que fazemos"), 33)
        self.assertEqual(vowel_count("BR"), 0)

    def test_function_vowel_count_return_integer(self):
        self.assertIsInstance(vowel_count("HBSIS"), int)
