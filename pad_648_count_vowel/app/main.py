def vowel_count(phrase: str):
    # Retorna a soma de ocorrência(s) de vogal(is) no parametro recebido
    return sum(phrase.count(i) for i in 'aeiou')

# Manual test
#print(vowel_count("We're here for tech and beer."))
