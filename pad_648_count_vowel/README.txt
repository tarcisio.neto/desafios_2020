Retorne o número (contagem) de vogais na sequência especificada.

Vamos considerar a, e, i, o e u como vogais para este Kata.

A sequência de entrada consistirá apenas de letras minúsculas e / ou espaços.