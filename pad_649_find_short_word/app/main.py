def find_short_word(phrase: str) -> int:
    list_words = phrase.split(" ")
    # A variável "palavra mais curta" recebe o item que possúi o menor número de caracteres
    shortest_word = min((w for w in list_words if w), key=len)
    # Retorna o tamanho em inteiros deste item
    return len(shortest_word)


# Manual Test
# print(find_short_word("HBSIS, a 35ª melhor empresa para se trabalhar"))
