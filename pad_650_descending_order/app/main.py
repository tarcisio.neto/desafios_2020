def descending_order(number: int) -> int:
    # Transformando a variável number(inteiro) em string e logo depois em lista
    number_list_on_string = list(str(number))
    # Ordenando os items da lista ao contrário (maior para menor)
    number_list_on_string.sort(reverse=True)
    # Agrupando os itens da lista em uma variável e a retornando como inteiro logo em seguida
    final_number = int(''.join(number_list_on_string))
    return final_number

# Teste
# print(descending_order(3425))
