from pad_650_descending_order.app.main import descending_order
import unittest


class TestMain(unittest.TestCase):
    def test_function_descending_order(self):
        self.assertEqual(descending_order(123), 321)
        self.assertEqual(descending_order(1256), 6521)
        self.assertEqual(descending_order(2547), 7542)

    def test_function_descending_order_return_integer(self):
        self.assertIsInstance(descending_order(123), int)
